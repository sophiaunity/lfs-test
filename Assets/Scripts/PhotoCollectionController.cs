﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class PhotoCollectionController : MonoBehaviour 
	{
		// public List<PhotoData> selectedPhotos = new List<PhotoData>();
		public static PhotoCollectionController _Instance;
		public GameObject photoRow;
		float height = 0;
		public RectTransform contentRect;

		List<GameObject> listOfRows;

		// Use this for initialization
		void OnEnable() 
		{
			GetPictures();
			
		}
		void Awake()
		{
			_Instance = this;
		}

		public void ResetList()
		{
			height = 0;
			foreach (GameObject go in listOfRows)
			{
				Destroy(go);
			}
		}

		public void GetPictures()
		{
			if (listOfRows != null)
			{
				ResetList();
			}
			
			listOfRows = new List<GameObject>();


			PhotoCollection._Instance.ResetCollection();
			PhotoCollection._Instance.SetPhotoCollectionController(true);
			int numOfPics = CameraRoll._Instance.CountList();

			int numOfRows = numOfPics/3;
			int remainder = numOfPics%3; //if this does not equal 0, add one row

			if (remainder != 0)
			{
				numOfRows += 1;
			}
			
			for (int i = 0; i < numOfPics; i += 3)
			{
				//Adds the pictures to the rows
				GameObject newObject = Instantiate(photoRow, new Vector3(0, 0, 0), Quaternion.identity, contentRect);
				newObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, height);
				listOfRows.Add(newObject);
				PhotoRow newRow = newObject.GetComponent<PhotoRow>();

				AnalyzedData photo1 = CameraRoll._Instance.GetPhoto(i);
				Texture2D textureA = null;
				if (photo1 != null)
				{
					textureA = photo1.rawPhoto;
				}

				AnalyzedData photo2 = CameraRoll._Instance.GetPhoto(i+1);
				Texture2D textureB = null;
				if (photo2 != null)
				{
					textureB = photo2.rawPhoto;
				}

				AnalyzedData photo3 = CameraRoll._Instance.GetPhoto(i+2);
				Texture2D textureC = null;
				if (photo3 != null)
				{
					textureC = photo3.rawPhoto;
				}

				if (!newRow.FillImages(textureA, i, textureB, i+1, textureC, i+2))
				{
					height = height - newObject.GetComponent<RectTransform>().sizeDelta.y;
					break;
				}

				height = height - newObject.GetComponent<RectTransform>().sizeDelta.y;
				

			}
			contentRect.sizeDelta = new Vector2(contentRect.sizeDelta.x, Mathf.Abs(height));
		}

		public void OnSubmitClick()
		{
			PhotoCollection._Instance.SetPhotoCollectionController(false);
			PhotoCollection._Instance.SetScoreScreenController(true);
		}
	}
}
