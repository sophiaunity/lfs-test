﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WildlifePhotography
{
	public class PhotoRow : MonoBehaviour 
	{
		public ImageBox[] imageBoxes;

		public bool FillImages(Texture2D a, int intA, Texture2D b, int intB, Texture2D c, int intC)
		{
			//fill the ui with the images, one row at a time in groups of 3
			if (a != null)
			{
				imageBoxes[0].rawImage.enabled = true;
				imageBoxes[0].rawImage.texture = a;	
				imageBoxes[0].cameraRollIndex = intA;	//This is so we will know which image is being selected
			}
			else
			{
				return false;
			}

			if (b != null)
			{
				imageBoxes[1].rawImage.enabled = true;
				imageBoxes[1].rawImage.texture = b;	
				imageBoxes[1].cameraRollIndex = intB;
			}
			else
			{
				return false;
			}

			if (c != null)
			{
				imageBoxes[2].rawImage.enabled = true;
				imageBoxes[2].rawImage.texture = c;		
				imageBoxes[2].cameraRollIndex = intC;	
			}
			else
			{
				return false;
			}

			return true;
		}

		public void OnPhotoSelect(int boxIndex)
		{

			imageBoxes[boxIndex].border.SetActive(!imageBoxes[boxIndex].border.activeSelf);
			imageBoxes[boxIndex].selected = !imageBoxes[boxIndex].selected;

			//Add to a list of selected photos
			if (imageBoxes[boxIndex].selected)
				PhotoCollection._Instance.SelectPhoto(imageBoxes[boxIndex].rawImage.texture as Texture2D, imageBoxes[boxIndex].cameraRollIndex);
			else
				PhotoCollection._Instance.DeselectPhoto(imageBoxes[boxIndex].cameraRollIndex);
		}

		[Serializable]
		public class ImageBox
		{
			public RawImage rawImage;
			public int cameraRollIndex;
			public GameObject border;

			public bool selected;

		}


	}
}
