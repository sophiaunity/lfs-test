﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	[RequireComponent(typeof(CameraCapture))]
	public class ImageAnalyzer : MonoBehaviour
	{
		public static ImageAnalyzer _Instance;

        public AnimalPhotoData photoData = new AnimalPhotoData();

        public float maxDistance = 30;

        Camera cam;

        void Awake()
		{
			_Instance = this;
            cam = GetComponent<Camera>();
		}

		public AnalyzedData AnalyzeImage(Texture2D texture)
		{
            photoData = AnimalManager._Instance.FindVisible(cam, maxDistance);

			AnalyzedData data = new AnalyzedData();

            //Position
            float distanceFromCenter = Vector2.Distance(photoData.screenPosition, new Vector2(0.5f, 0.5f));
            data.distanceFromPoint = Mathf.Clamp(100-((distanceFromCenter/7.1f)*1000), 0, 100);
            //Size
            Vector2 normalizedScreenPos = new Vector2(photoData.screenSpaceRect.width / cam.pixelWidth, photoData.screenSpaceRect.width / cam.pixelWidth);
            float squares = (normalizedScreenPos.x * normalizedScreenPos.x) + (normalizedScreenPos.y * normalizedScreenPos.y);
            float hypSize = Mathf.Sqrt(squares);
            float screenHyp = 1.41f;
            float size = (hypSize / screenHyp) * 100;
            data.screenPercentage = Mathf.Clamp(size, 0, 100); //Larger than the image return wonky values, fix this
            //Temp
			data.animalType = new MalbersAnimations.Animal();
            data.focalPercentage = 100;
            data.action = 100;
            data.rawPhoto = texture;
            //Animal type
            Vector3 direction = this.transform.position - photoData.worldPosition;

            RaycastHit hit;
            Ray ray = new Ray(transform.position, direction);

            if (Physics.Raycast(ray, out hit, 200))
            {
                if (hit.collider.tag == "Animal")
                {
                   // hit.collider.GetComponent<MalbersAnimation.Animal>().isHit = true;
                }
            }
            //data.focalPercentage = UnityEngine.Random.Range(0, 100);
            //data.action = UnityEngine.Random.Range(0, 100);
            //data.rawPhoto = texture;

            return data;
		}

	}

	[Serializable]
	public class AnalyzedData
	{
		public int cameraRollIndex;
		public float screenPercentage;
		public float distanceFromPoint;
		public MalbersAnimations.Animal animalType;
		public float focalPercentage;
		public int action;
		public Texture2D rawPhoto;
	}
}