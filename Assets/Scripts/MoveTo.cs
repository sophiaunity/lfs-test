﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

namespace WildlifePhotography
{
	public class MoveTo : MonoBehaviour
	{
		
		public Transform goal;
		
		public NavMeshAgent agent;
		public GameObject photoCollectionController;
		//NavMeshAgent agent = GetComponent<NavMeshAgent>();
		
		void OnEnable()
		{
			//NavMeshAgent agent = GetComponent<NavMeshAgent>();
			StartPath();

			//if (agent.transform.position == goal.) 
		}

		public void StartPath()
		{
			agent.destination = goal.position;
		}

		void OnTriggerEnter(Collider coll)
		{
			//ends the level when goal is reached
			if (coll.transform.CompareTag("Goal"))
			{
				EndLevel();
			}
			
		}

		void EndLevel()
		{
			agent.Stop();
			agent.ResetPath();
			Globals._Instance.PhotoCollectionUI.gameObject.SetActive(true);
			photoCollectionController.gameObject.SetActive(true);

			//Debug.Log("UI enabled");
			PlayerController._Instance.DisableControl();
			//Debug.Log("Mouse disabled");

		}
	}
}