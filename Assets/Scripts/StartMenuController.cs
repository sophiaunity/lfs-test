﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WildlifePhotography
{
	public class StartMenuController : MonoBehaviour
	{
		public static StartMenuController _Instance;
		public Camera startMenuCamera;
		public PlayerController playerController;
		public Button btnContinue;
		public Globals globals;

		void Start()
		{
			InitializeStartMenu();
		}

		void Awake()
		{
			_Instance = this;
		}

		public void OnNewGameClick()
		{
			CameraRoll._Instance.ClearList();
			PlayerSettings._Instance.playerCash = 0;
			DisableStartMenuCamera();

			playerController.gameObject.SetActive(true);
			playerController.Reset();
			PlayerSettings._Instance.UpdatePlayerCash(0);
		}

		public void OnContinueClick()
		{
			CameraRoll._Instance.ClearList();
			DisableStartMenuCamera();
			
			playerController.gameObject.SetActive(true);
			playerController.Reset();
		}

		void DisableStartMenuCamera()
		{
			startMenuCamera.gameObject.SetActive(false);
		}

		public void InitializeStartMenu()
		{
			//if the player has no existing cash, 'Continue' is hidden
			startMenuCamera.gameObject.SetActive(true);

			if (PlayerSettings._Instance.playerCash == 0)
			{
				btnContinue.gameObject.SetActive(false);
			}
			else
			{
				btnContinue.gameObject.SetActive(true);
			}
		}
	}
}