﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace WildlifePhotography
{
	public class PlayerController : MonoBehaviour 
	{
		public MouseLook mouseLook;
		public Camera mainCamera;
        CameraCapture capture;
		public PhotoCamera photoCamera;
        public GameObject cameraUI;
		bool captureActive;
		NavMeshAgent agent;
        public UnityEngine.PostProcessing.PostProcessingProfile eyeProfile;
        public UnityEngine.PostProcessing.PostProcessingProfile cameraProfile;
		public GameObject lensObject;
        public static PlayerController _Instance;
        bool doubleSpeed = false;

		void Update()
		{
			if (Input.GetButtonUp("Fire2"))
			{
				SwitchCamera();
			}
            if(Input.GetKeyUp(KeyCode.Space))
            {
                SwitchSpeed();
            }
		}

        void SwitchSpeed()
        {
            doubleSpeed = !doubleSpeed;
            if(doubleSpeed)
            {
                agent.speed = 6;
            }
            else
            {
                agent.speed = 3;
            }
        }

		void Awake()
		{
			_Instance = this;
            agent = GetComponent<NavMeshAgent>();
            capture = mainCamera.GetComponent<CameraCapture>();
		}

		public void Reset()
		{
			capture.ResetIndex();
		}

		void SwitchCamera()
		{
            
			captureActive = !captureActive;
            if (captureActive)
			{
				EnablePhotoCamera();
				//DisableEyesCamera();
			}
			else
			{
				//EnableEyesCamera();
				DisablePhotoCamera();
			}
		}

		void EnableEyesCamera()
		{
			mainCamera.gameObject.SetActive(true);
			resume();
		}

		void EnablePhotoCamera()
		{
            //stop moving

            cameraUI.SetActive(true);
            capture.SwitchCamera(true);
			photoCamera.enabled = true;
            mainCamera.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile = cameraProfile;
            pause();
		}

		void DisableEyesCamera()
		{
			mainCamera.gameObject.SetActive(false);
			pause();
		}

		void DisablePhotoCamera()
		{
			//stop moving

            cameraUI.SetActive(false);
            capture.SwitchCamera(false);
			photoCamera.enabled = false;
			lensObject.transform.localPosition = Vector3.zero;
            mainCamera.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile = eyeProfile;
            resume();
		}

		public void DisableControl()
		{
			mouseLook.lockCursor = false;
			Debug.Log("Lock Cursor disabled");
		}

		void pause()
		{
			//sets the NavMeshAgent speed to 0 when camera is out
			agent.speed = 0;
     	}
     
		void resume()
		{
			//sets the NavMeshAgent speed to 2 when the camera is put away
			agent.speed = 2;
		}

	}
}
