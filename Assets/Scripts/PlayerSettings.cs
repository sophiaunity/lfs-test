﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class PlayerSettings : MonoBehaviour 
	{
		public static PlayerSettings _Instance;
		public float playerCash;

		void Awake()
		{
			_Instance = this;
		}

		public float GetPlayerCash()
		{
			return playerCash;
		}

		public void UpdatePlayerCash(float inputCash)
		{
			playerCash = inputCash;
		}
	}
}