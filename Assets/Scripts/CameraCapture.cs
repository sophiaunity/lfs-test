﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace WildlifePhotography
{
	[RequireComponent(typeof(Camera))]
	public class CameraCapture : MonoBehaviour
	{
		public static CameraCapture _Instance;
		bool isCapturing;
		public RenderTexture captureTexture;
		Camera cam;
        // public List<Texture2D> listOfTextures;
        //CameraRoll m_CameraRoll;
        bool allowCapture;
		int photoIndex;

		// Use this for initialization
		void Start ()
		{
			cam = GetComponent<Camera>();
			SetUpRenderTexture();
			CameraRoll._Instance.CreateList();

		}
		void Awake()
		{
			_Instance = this;
		}

		public void ResetIndex()
		{
			photoIndex = 0;
		}

        public void SwitchCamera(bool active)
        {
            allowCapture = active;
        }
		
		// Update is called once per frame
		void Update ()
		{
            if (allowCapture)
            {
                if (Input.GetButtonUp("Fire1"))
                {
                    isCapturing = true;
                }
            }
		}

		void SetUpRenderTexture()
		{
			captureTexture = new RenderTexture((int)cam.pixelWidth, (int)cam.pixelHeight, 24, RenderTextureFormat.ARGB32);
			captureTexture.Create();
		}

		void OnRenderImage(RenderTexture src, RenderTexture dst)
		{		
			Graphics.Blit(src, dst);
			if (isCapturing == true)
			{
				isCapturing = false;
				Graphics.Blit(src, captureTexture);
				StartCoroutine(ProcessImage());
			}

		}

		IEnumerator ProcessImage()
		{
			//takes thes screenshot and adds it to list
			string textureName = "screenshot";
			Texture2D texture = ConvertRenderTextureToTexture2D(textureName, captureTexture, new Vector2(captureTexture.width, captureTexture.height), TextureFormat.RGB24, FilterMode.Bilinear);
			do { yield return null; } while (texture == null);
			//listOfTextures.Add(texture);
			AnalyzedData data = ImageAnalyzer._Instance.AnalyzeImage(texture);
			data.cameraRollIndex = photoIndex;
			do { yield return null; } while (data == null);
			if (!CameraRoll._Instance.AddToList(data))
			{
				Debug.Log("List is not initialised");
			}
			//RenderTexture.Destroy(captureTexture);
			// ImageAnalyzer._Instance.AnalyzeImage(texture);
			photoIndex++;
		}

		public static Texture2D ConvertRenderTextureToTexture2D(string textureName, RenderTexture input, Vector2 resolution, TextureFormat format, FilterMode filterMode)
		{
			RenderTexture.active = input;
			Texture2D output = new Texture2D((int)resolution.x, (int)resolution.y, format, false);
			output.name = textureName;
			output.filterMode = filterMode;
			output.ReadPixels(new Rect(0, 0, (int)resolution.x, (int)resolution.y), 0, 0);
			output.Apply();
			RenderTexture.active = null;
			return output;
		}

	}
}