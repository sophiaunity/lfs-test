﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class ScoreCalculator : MonoBehaviour 
	{
		public static ScoreCalculator _Instance;

		void Awake()
		{
			_Instance = this;
		}

		public PhotoData CalculateScore(AnalyzedData rawData)
		{
			//This is random for the moment

			PhotoData processedData = new PhotoData();

			processedData.animal = "Fox";
			processedData.location = gameObject.scene.name.ToString();
			processedData.quality = rawData.focalPercentage;
			processedData.comp = rawData.distanceFromPoint;
			processedData.size = rawData.screenPercentage;
			processedData.behaviour = rawData.action;

			return processedData;
		}
	}
}