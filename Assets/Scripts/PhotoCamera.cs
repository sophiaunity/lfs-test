﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class PhotoCamera : MonoBehaviour 
	{
		public GameObject lensObject;
		public float cameraDistanceMax = 10f;
		float cameraDistanceMin = 0f;
		float cameraDistance = 0f;
		public float scrollSpeed = 0.5f;
		
		void Update()
		{
			//Zoom with mouse wheel
			cameraDistance += Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
			cameraDistance = Mathf.Clamp(cameraDistance, cameraDistanceMin, cameraDistanceMax);
			lensObject.transform.localPosition = new Vector3(0,0,cameraDistance);
		}
	}
}