﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class CameraRoll : MonoBehaviour 
	{
		public static CameraRoll _Instance;
		public List<AnalyzedData> listOfData;
		
		void Awake()
		{
			_Instance = this;
		}

		public void CreateList()
		{
			listOfData = new List<AnalyzedData>();
		}

		public bool AddToList(AnalyzedData data)
		{
			if (listOfData != null)
			{
				listOfData.Add(data);
				return true;
			}
			else
			{
				return false;
			}
		}

		public void ClearList()
		{
			//Debug.Log("CLEAR");
			listOfData.Clear();
		}

		public int CountList()
		{
			return listOfData.Count;		
		}

		public AnalyzedData GetPhoto(int index)
		{
			if (index < listOfData.Count)
			{
				return listOfData[index];
			}
			else
			{
				return null;
			}
		}
	}
}