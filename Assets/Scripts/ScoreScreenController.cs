﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WildlifePhotography
{
	public class ScoreScreenController : MonoBehaviour
	{
        //Singletons
		public static ScoreScreenController _Instance;
        //UI
		public RawImage currentTexture;
		public Text photoTitle;
		public Text location;
		public Text rating;
		public Text sizeScore;
		public Text compScore;
		public Text qualityScore;
		public Text behaviourScore;
		public Text payout;
        public Button btnNext;
        public Button btnFinish;
        //Data
        int collectionSearchIndex;
		public float cash = 0;
        //Objects
		public Transform startPosition;
		public Camera startMenuCamera;

		void Awake()
		{
			_Instance = this;
		}

		void OnEnable() 
		{
			collectionSearchIndex = 0;
			PopulateScreen();		
			btnFinish.gameObject.SetActive(false);
			btnNext.gameObject.SetActive(true);	
		}

		void PopulateScreen()
		{
			//Fills the score screen with text and image
			AnalyzedData currentPhoto = PhotoCollection._Instance.GetPhoto(collectionSearchIndex);
			currentTexture.texture = currentPhoto.rawPhoto;

			PhotoData currentScores = ScoreCalculator._Instance.CalculateScore(currentPhoto);

			PopulateText(currentScores);
			PlayerSettings._Instance.GetPlayerCash();

			collectionSearchIndex++;
		}

		void PopulateText(PhotoData photoData)
		{
			photoTitle.text = photoData.animal;
			location.text = photoData.location;
			sizeScore.text = photoData.size.ToString() + "%";
			compScore.text = photoData.comp.ToString() + "%";
			qualityScore.text = photoData.quality.ToString() + "%";
			behaviourScore.text = photoData.behaviour.ToString() + "%";

			float ratingNumber = CalculateRating(photoData);
			rating.text = ratingNumber.ToString() + "%";

			payout.text = "£" + CalculatePayout(ratingNumber).ToString();
		}

		float CalculateRating(PhotoData currentScores)
		{
			//Averages the values
			float finalScoreSize = currentScores.size;
			float finalScoreComp = currentScores.comp;
			float finalScoreQuality = currentScores.quality;
			float finalScoreBehaviour = currentScores.behaviour;

			float finalRating = (finalScoreSize + finalScoreComp + finalScoreQuality + finalScoreBehaviour) /4;
			return finalRating;
		}

		float CalculatePayout(float ratingScore)
		{
			cash = cash + ratingScore*10;
			return cash;
		}

		public void OnNextClick()
		{
			if (collectionSearchIndex < PhotoCollection._Instance.selectedPhotos.Count)
			{
				PopulateScreen();
			}
			else
			{
				btnFinish.gameObject.SetActive(true);
				btnNext.gameObject.SetActive(false);
				//on the click of that button, go to start screen
			}
		}

		public void OnFinishClick()
		{
			PlayerSettings._Instance.UpdatePlayerCash(cash);
			Globals._Instance.playerController.transform.position = startPosition.position;
            Globals._Instance.playerController.gameObject.SetActive(false);
            Globals._Instance.startMenu.gameObject.SetActive(true);
            Globals._Instance.startMenu.InitializeStartMenu();
            Globals._Instance.photoCollectionController.gameObject.SetActive(false);
            Globals._Instance.photoCollection.gameObject.SetActive(false);
			this.gameObject.SetActive(false);
		}
	}
}