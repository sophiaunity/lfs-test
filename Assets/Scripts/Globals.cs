﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildlifePhotography
{
	public class Globals : MonoBehaviour 
	{
		public static Globals _Instance;
		public GameObject PhotoCollectionUI;
		PlayerSettings player;
        public StartMenuController startMenu;
        public PlayerController playerController;
        public PhotoCollection photoCollection;
        public PhotoCollectionController photoCollectionController;

		void Awake()
		{
			_Instance = this;
            player = GetComponent<PlayerSettings>();
		}
	}
}