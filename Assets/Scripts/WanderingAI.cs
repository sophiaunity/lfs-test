﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
 
public class WanderingAI : MonoBehaviour {
 
    public float wanderRadius;
    public float wanderTimer;
 
    private Transform target;
    private NavMeshAgent agent;
    private float timer;
    Rigidbody bodyOfAgent;
    Vector3 angularVelocity;
 
    // Use this for initialization
    void OnEnable () {
        agent = GetComponent<NavMeshAgent> ();
        bodyOfAgent = GetComponent<Rigidbody>();
        timer = wanderTimer;
    }
 
    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
 
        if (timer >= wanderTimer) {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            agent.SetDestination(newPos);
            timer = 0;
        }
    }
 
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;

        Vector3 positiveDirection = new Vector3(randDirection.x, randDirection.y, Mathf.Abs(randDirection.z)+5);
 
        NavMeshHit navHit;
 
        NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }

    public Vector3 GetAngularVelocity()
    {
        angularVelocity = bodyOfAgent.angularVelocity;
        return angularVelocity;
    }

    public Vector3 GetVelocity()
    {
        return bodyOfAgent.velocity;
    }
}