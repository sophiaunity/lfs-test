﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MalbersAnimations;

namespace WildlifePhotography
{
    public class AnimalManager : MonoBehaviour
    {
        public static AnimalManager _Instance;

        public List<MalbersAnimations.Animal> animals = new List<MalbersAnimations.Animal>();

        public AnimalPhotoData temp;

        private void Awake()
        {
            _Instance = this;
        }

        public AnimalPhotoData FindVisible(Camera cam, float maxDistance)
        {
            List<AnimalPhotoData> photoDatas = new List<AnimalPhotoData>();
            for(int i = 0; i < animals.Count; i++)
            {
                MalbersAnimations.MalbersNavInput nav = animals[i].GetComponent<MalbersAnimations.MalbersNavInput>();
                if (nav.renderer.isVisible)
                {
                    float distance = Vector3.Distance(cam.transform.position, nav.transform.position);
                    if (distance < maxDistance)
                    {
                        AnimalPhotoData newData = new AnimalPhotoData();
                        //Distance and position
                        newData.worldPosition = nav.transform.position;
                        Vector2 screenPosPixels = cam.WorldToScreenPoint(newData.worldPosition);
                        newData.screenPosition = new Vector2(screenPosPixels.x / cam.pixelWidth, screenPosPixels.y / cam.pixelHeight);
                        newData.distanceToCamera = distance;
                        //Size
                        Rect rect = GUIRectWithObject(nav.renderer.gameObject, cam);
                        newData.screenSpaceRect = rect;
                        photoDatas.Add(newData);
                    }
                }
            }
            photoDatas.Sort((x, y) => x.distanceToCamera.CompareTo(y.distanceToCamera));
            return photoDatas[0];
        }

        //Get rect size
        public static Rect GUIRectWithObject(GameObject go, Camera cam)
        {
            Vector3 cen = go.GetComponent<Renderer>().bounds.center;
            Vector3 ext = go.GetComponent<Renderer>().bounds.extents;
            Vector2[] extentPoints = new Vector2[8]
             {
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z), cam),
               WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z), cam)
             };
            Vector2 min = extentPoints[0];
            Vector2 max = extentPoints[0];
            foreach (Vector2 v in extentPoints)
            {
                min = Vector2.Min(min, v);
                max = Vector2.Max(max, v);
            }
            return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
        }

        public static Vector2 WorldToGUIPoint(Vector3 world, Camera cam)
        {
            Vector2 screenPoint = cam.WorldToScreenPoint(world);
            screenPoint.y = (float)Screen.height - screenPoint.y;
            return screenPoint;
        }

    }

    [System.Serializable]
    public class AnimalPhotoData
    {
        public Vector3 worldPosition;
        public Vector2 screenPosition;
        public float distanceToCamera;
        public Rect screenSpaceRect;
    }
}


