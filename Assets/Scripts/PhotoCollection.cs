﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WildlifePhotography
{
	public class PhotoCollection : MonoBehaviour 
	{
		// public GameObject photoRow;
		// float height = 0;
		// public RectTransform contentRect;
		public static PhotoCollection _Instance;
		//public List<PhotoData> selectedPhotos = new List<PhotoData>();

		public List<AnalyzedData> selectedPhotos = new List<AnalyzedData>();

		public GameObject photoCollectionController;
		public GameObject scoreScreenController;

		void Awake()
		{
			_Instance = this;
		}

		public void SetPhotoCollectionController(bool active)
		{
			photoCollectionController.SetActive(active);
		}
		public void SetScoreScreenController(bool active)
		{
			scoreScreenController.SetActive(active);
		}

		public AnalyzedData GetPhoto(int indexInList)
		{
			return selectedPhotos[indexInList];
		}

		public void ResetCollection()
		{
			selectedPhotos.Clear();
		}
		
		public void SelectPhoto(Texture2D photo, int index)
		{
			//Click on the UI
			selectedPhotos.Add(CameraRoll._Instance.listOfData[index]);
		}

		public void DeselectPhoto(int indexOfTexture)
		{
			for (int i = 0; i<selectedPhotos.Count; i++)
			{
				if (selectedPhotos[i].cameraRollIndex == indexOfTexture)
				{
					selectedPhotos.RemoveAt(i);
				}
			}
		}
	}

	
	[Serializable]
	public class PhotoData
	{
		public Texture2D photo;
		public int cameraRollIndex;

		public string animal;

		public string location;

		public float size;

		public float comp;

		public float quality;

		public float behaviour;


	}

}
