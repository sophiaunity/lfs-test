﻿using UnityEngine;
using System.Collections.Generic;
using System;
#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

namespace MalbersAnimations
{
    public class MalbersNavInput : MonoBehaviour
    {
        private iMalbersInputs character;
        private Vector3 m_CamForward;
        private Vector3 m_Move;
        private Transform m_Cam;
        public List<InputRow> inputs = new List<InputRow>();
        public bool cameraBaseInput;
        private float horizontal;  //Horizontal Right & Left   Axis X
        private float vertical;  //Vertical   Forward & Back Axis Z

        public Renderer renderer;

        WanderingAI wander;

        void Awake()
        {
            //get the animalScript
            character = GetComponent<Animal>();
            wander = GetComponent<WanderingAI>();
        }

        private void Start()
        {
            if (Camera.main != null)   // get the transform of the main camera
                m_Cam = Camera.main.transform;
        }


        // Fixed update is called in sync with physics
        void FixedUpdate()
        {
#if !CROSS_PLATFORM_INPUT
            //horizontal = Input.GetAxis("Horizontal");
            //vertical = Input.GetAxis("Vertical");

            horizontal = wander.GetVelocity().y;
            //vertical = wander.GetAngularVelocity().x * 10;
            //vertical = transform.InverseTransformDirection(wander.GetVelocity()).x;
            vertical = transform.InverseTransformDirection(GetComponent<UnityEngine.AI.NavMeshAgent>().velocity).x;

            //Debug.Log("h: " + horizontal + " v: " + vertical);
#else
            h = CrossPlatformInputManager.GetAxis("Horizontal");
            v = CrossPlatformInputManager.GetAxis("Vertical");
#endif
            SetInput();
        }

      public virtual Vector3 CameraInputBased()
        {
            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 1, 1)).normalized;
                m_Move = vertical * m_CamForward + horizontal * m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = vertical * Vector3.forward + horizontal * Vector3.right;
            }
            return m_Move;
        }

        /// <summary>
        /// Send all the Inputs to the Animal
        /// </summary>
        protected virtual void SetInput()
        {
            if (cameraBaseInput)
            {
                character.Move(CameraInputBased());
            }
            else
            {
                character.Move(new Vector3(horizontal, 0, vertical), false);
            }

            if (isActive("Attack1")) character.Attack1 = GetInput("Attack1");         //Get the Attack1 button
            if (isActive("Attack2")) character.Attack2 = GetInput("Attack2");         //Get the Attack1 button

            if (isActive("Action")) character.Action = GetInput("Action");  //Get the Action/Emotion button

            if (isActive("Jump")) character.Jump = GetInput("Jump");

            if (isActive("Shift")) character.Shift = GetInput("Shift");           //Get the Shift button

            if (isActive("Fly")) character.Fly = GetInput("Fly");                //Get the Fly button 
            if (isActive("Down")) character.Down = GetInput("Down");             //Get the Down button
            if (isActive("Dodge")) character.Dodge = GetInput("Dodge");             //Get the Down button

            if (isActive("Stun")) character.Stun = GetInput("Stun");             //Get the Stun button change the variable entry to manipulate how the stun works
            if (isActive("Death")) character.Death = GetInput("Death");            //Get the Death button change the variable entry to manipulate how the death works
            if (isActive("Damaged")) character.Damaged = GetInput("Damaged");

            if (isActive("Speed1")) character.Speed1 = GetInput("Speed1");                //Walk
            if (isActive("Speed2")) character.Speed2 = GetInput("Speed2");                //Trot
            if (isActive("Speed3")) character.Speed3 = GetInput("Speed3");                //Run

            //Get the Death button change the variable entry to manipulate how the death works
        }

        /// <summary>
        /// Enable/Disable the Input
        /// </summary>
        public void EnableInput(string inputName, bool value)
        {
            foreach (InputRow item in inputs)
            {
                if (item.name.ToUpper() == inputName.ToUpper())
                {
                    item.active = value;
                    return;
                }
            }
        }

        /// <summary>
        /// Thit will set the correct Input, from the Unity Input Manager or Keyboard.. you can always modify this code
        /// </summary>
        protected bool GetInput(string name)
        {
            foreach (InputRow item in inputs)
            {
                if (item.name.ToUpper() == name.ToUpper() && item.active)
                {
                    return item.GetInput;
                }
            }
            return false;
        }

        /// <summary>
        /// Check if the input is active
        /// </summary>
        bool isActive(string name)
        {
            foreach (InputRow item in inputs)
            {
                if (item.name.ToUpper() == name.ToUpper()) return item.active;
            }
            return false;
        }
    }
}